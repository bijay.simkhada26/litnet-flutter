import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    QuillController _controller = QuillController.basic();

    return Container(
      padding: const EdgeInsets.all(15),
      margin: const EdgeInsets.all(15),
      decoration: BoxDecoration(
          color: Colors.white,
        border: Border.all(
          color: Colors.black,
          width: 1
        ),
        borderRadius: BorderRadius.circular(15.0)
      ),
      child: Column(
        children: [
          QuillToolbar.basic(controller: _controller),
          Expanded(
            child: Container(
              child: QuillEditor.basic(
                controller: _controller,
                readOnly: false, // true for view only mode
              ),
            ),
          )
        ],
      )
    );
  }
}
