import 'package:flutter/material.dart';
import 'package:litnet_flutter/modules/home/homeScreen.dart';
import 'package:litnet_flutter/partials/BottomNav.dart';
import 'package:litnet_flutter/partials/TopNav.dart';

class AppLayout extends StatefulWidget {
  const AppLayout({Key? key}) : super(key: key);

  @override
  State<AppLayout> createState() => _AppLayoutState();
}

class _AppLayoutState extends State<AppLayout> {

  final List<Widget> _pages = [
    const HomeScreen(),
    const HomeScreen(),
    const HomeScreen(),
    const HomeScreen(),
  ];

  int _selectedIndex = 0;

  void _changeSelectedIndex(index){
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const Drawer(),
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: TopNav(),
      ),
      body: _pages[_selectedIndex],
      bottomNavigationBar: BottomNav(_selectedIndex, _changeSelectedIndex),
    );
  }
}
