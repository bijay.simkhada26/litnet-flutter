import 'package:flutter/material.dart';

class BottomNav extends StatelessWidget {

  final int _selectedIndex;
  final Function _changeIndex;

  const BottomNav( this._selectedIndex, this._changeIndex, {Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black54,
            spreadRadius: 1,
            blurRadius: 8,
          )
        ],
        borderRadius: BorderRadius.only(topLeft: Radius.circular(40), topRight: Radius.circular(40))
      ),
      child: ClipRRect(
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(40), topRight: Radius.circular(40)),
          child: BottomNavigationBar(
            elevation: 5,
            showUnselectedLabels: false,
            showSelectedLabels: true,
            unselectedItemColor: Colors.black54,
            selectedItemColor: Colors.black,
            currentIndex: _selectedIndex,
            iconSize: 30,
            onTap: (i){
              _changeIndex(i);
            },
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(icon: Icon(Icons.home_outlined), label: 'Home', activeIcon: Icon(Icons.home, color: Colors.deepOrange,)),
              BottomNavigationBarItem(icon: Icon(Icons.category_outlined), label: 'Category', activeIcon: Icon(Icons.category, color: Colors.green,)),
              BottomNavigationBarItem(icon: Icon(Icons.search_outlined), label: 'Search', activeIcon: Icon(Icons.search, color: Colors.blueAccent,)),
              BottomNavigationBarItem(icon: Icon(Icons.person_outline), label: 'Profile', activeIcon: Icon(Icons.person, color: Colors.redAccent,)),
            ]
        ),
      ),
    );
  }
}
