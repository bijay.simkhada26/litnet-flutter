import 'package:flutter/material.dart';
import 'package:litnet_flutter/utils/theme.dart';

class TopNav extends StatelessWidget {
  const TopNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      iconTheme: const IconThemeData(
        color: Colors.black87
      ),
      title: Text('LitNet', style: kHeading1,),
      actions: [
        CircleAvatar(
          radius: 25,
          child: Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiGcFYBKGruads8sUVAfUBlX8orSdEwuSSTg&usqp=CAU"),
        )
      ],
    );
  }
}
